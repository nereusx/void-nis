#!/bin/sh

( cd libnsl && make clean )
( cd libnss_nis && make clean )
( cd ypbind-mt && make clean )
( cd ypserv && make clean )
( cd yp-tools && make clean )

# copy /etc
#cp -r etc/* /etc
