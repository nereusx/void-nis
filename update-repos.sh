#!/bin/sh

updgit() {
	if [ -d "$1" ]; then
		cd "$1"
		git pull
		cd ..
	else
		git clone https://github.com/thkukuk/$1
	fi
	}

updgit libnsl
updgit libnss_nis
updgit yp-tools
updgit ypbind-mt
updgit ypserv

usr=$(id -un)
grp=$(id -gn)
chown -R $usr:$grp *
