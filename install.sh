#!/bin/sh

pro() {
	echo "=== $1"
	if cd $1; then
		if command -vp autoupdate > /dev/null 2>&1; then
			autoupdate
		fi
		if [ -x ./autogen.sh ]; then
			./autogen.sh
		fi
		if ./configure > /dev/null; then
			if make > /dev/null; then
				make install > /dev/null
			else
				echo "--- make error"
				exit 1
			fi
		else
			echo "--- configure error"
			exit 1
		fi
		cd ..
	else
		echo "--- $1 direcotry not found"
		exit 1
	fi
	}

pro libnsl
pro libnss_nis
ldconfig
pro ypbind-mt
pro ypserv
pro yp-tools

# copy /etc
#cp -r etc/* /etc
