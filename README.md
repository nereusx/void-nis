# void-nis

**NIS** support for void linux.

There is no more easiest way to setup or manage Unix LAN.

> Network Information System (NIS) is designed to centralize administration of UNIX®-like systems such as Solaris™, HP-UX, AIX®, Linux, NetBSD, OpenBSD, and FreeBSD. NIS was originally known as Yellow Pages but the name was changed due to trademark issues.
> This is the reason why NIS commands begin with yp.
>
> NIS is a Remote Procedure Call (RPC)-based client/server system that allows a group of machines within an NIS domain to share a
common set of configuration files. This permits a system administrator to set up NIS client systems with only minimal
configuration data and to add, remove, or modify configuration data from a single location.
>
> FreeBSD Handbook (of 14.1 release - 2024)

https://docs.freebsd.org/en/books/handbook/network-servers/

## Resources

[YoLinux NIS Tutorial](http://www.yolinux.com/TUTORIALS/NIS.html)

[The Linux NIS(YP) How-To](https://tldp.org/HOWTO/NIS-HOWTO/)

[ArchWiki NIS](https://wiki.archlinux.org/title/NIS)

[Manage NFS and NIS](https://reference-library.vercel.app/ror/nfs/index.htm)

## Required

* libcrypt

It is in glibc.
```
xbps-install glibc-devel gettext-devel
```

* libnss
```
xbps-install nss nss-devel
```

* libtirpc
```
xbps-install libtirpc libtirpc-devel
```

* NSL library (version 2 included)
```
xbps-install libnsl libnsl-devel
```

